import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:notifwp/modules/login/views/login.views.dart';
import 'package:notifwp/modules/main/controllers/main.controllers.dart';
import 'package:notifwp/modules/nofitikasi/views/notifikasi.views.dart';
import 'package:notifwp/ui/Uicolor.dart';
import 'package:onesignal_flutter/onesignal_flutter.dart';

class MainViews extends StatefulWidget {
  @override
  _MainViewsState createState() => _MainViewsState();
}

class _MainViewsState extends State<MainViews> {
  MainControllers controllers;
  String title = "";
  String content = "";
  Map<String, dynamic> data = {};

  @override
  void initState() {
    super.initState();
    controllers = new MainControllers();
    controllers.checkSudahLogin();
    controllers.getSubscriptionsId();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: GetBuilder<MainControllers>(
        init: controllers,
        builder: (params){
          if(params.hasLogin){
            return NotifikasiViews();
          }
          return LoginViews();

        },
      ),
    );
    // return !controllers.hasLogin ? LoginViews() : NotifikasiViews();
  }
}
