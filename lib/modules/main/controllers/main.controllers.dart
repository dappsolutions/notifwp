

import 'package:get/get.dart';
import 'package:onesignal_flutter/onesignal_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MainControllers extends GetxController{
  bool hasLogin = false;
  String OneSignalIdPlayer = "";


  void checkSudahLogin() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();

    this.hasLogin = prefs.getString("user_id") != null ? true : false;

    // String deviceId = await PlatFormDev
    // print();
    this.update();
  }

  void getSubscriptionsId() async{
    var status = await OneSignal.shared.getPermissionSubscriptionState();
    var playerId = "";
    if(status.subscriptionStatus.subscribed){
      playerId = status.subscriptionStatus.userId;
    }

    this.OneSignalIdPlayer = playerId;
    this.update();
  }
}