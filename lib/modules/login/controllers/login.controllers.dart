
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:notifwp/config/api.config.dart';
import 'package:notifwp/config/modules.config.dart';
import 'dart:convert';

import 'package:notifwp/config/routes.config.dart';
import 'package:notifwp/modules/login/models/login.models.dart';
import 'package:notifwp/modules/nofitikasi/views/notifikasi.views.dart';
import 'package:notifwp/modules/response/models/message.models.dart';
import 'package:onesignal_flutter/onesignal_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginController extends GetxController{
  bool loadingLogin = false;
  String OneSignalIdPlayer = "";

  @override
  void onInit() {
    super.onInit();
  }

  void signIn(username, pass, context) async{
    this.loadingLogin = true;
    update();

    await getSubscriptionsId();
    // return;
    Map<String, dynamic> params = {};
    params["username"] = username;
    params["pass"] = pass;
    params["player_id"] = this.OneSignalIdPlayer;


    var dio = new Dio();
    var request = await dio.post(Api.route[ModulesConfig.MODULE_LOGIN][Routes.SIGN_IN], queryParameters: params);

    if(request.statusCode == 200){
      this.loadingLogin = false;
      update();

      var data = json.decode(request.data);
      // print("DATA ${data}");
      MessageModel respMessage = MessageModel.fromJson(data);
      if(respMessage.is_valid == "1"){
        LoginModel loginModel = LoginModel.fromJson(data["data"]);
        SharedPreferences prefs = await SharedPreferences.getInstance();
        prefs.setString("username", loginModel.email);
        prefs.setString("user_id", loginModel.user);

        Get.snackbar("Informasi", respMessage.message, backgroundColor: Colors.green, colorText: Colors.white);
        Navigator.of(context).pop();
        Get.to(NotifikasiViews());
      }else{
        Get.snackbar("Informasi", respMessage.message, backgroundColor: Colors.redAccent, colorText: Colors.white);
      }
    }else{
      this.loadingLogin = false;
      update();

      Get.snackbar("Informasi", "Gagal Memuat Data", backgroundColor: Colors.redAccent, colorText: Colors.white);
    }
  }


  void getSubscriptionsId() async{
    var status = await OneSignal.shared.getPermissionSubscriptionState();
    var playerId = "";
    // if(status.subscriptionStatus.subscribed){
      playerId = status.subscriptionStatus.userId;
    // }

    print("PLAYER ID ${playerId}");

    this.OneSignalIdPlayer = playerId;
    this.update();
  }
}