
class LoginModel{
  String id;
  String user;
  String apps;
  String createddate;
  String name;
  String email;

  LoginModel({this.id, this.user, this.apps, this.createddate, this.name, this.email});

  LoginModel.fromJson(Map<String, dynamic> json){
    this.id = json['id'];
    this.user = json['user'];
    this.apps = json['apps'];
    this.createddate = json['createddate'];
    this.name = json['name'];
    this.email = json['email'];
  }
}