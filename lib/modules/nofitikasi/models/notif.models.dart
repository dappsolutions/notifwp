

class NotifModel {
  String id;
  String status;
  String content;
  String data;
  String to;
  String createddate;
  NotifModel({this.id, this.status, this.content,
    this.data,
    this.to, this.createddate});


  NotifModel.fromJson(Map<String, dynamic> json){
    this.id = json['id'].toString();
    this.status = json['status'].toString();
    this.content = json['content'].toString();
    this.data = json['data'];
    this.to = json['to'].toString();
    this.createddate = json['createddate'].toString();
  }

  Map<String, dynamic> toJson(){
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['status'] = this.status;
    data['content'] = this.content;
    data['data'] = this.data;
    data['to'] = this.to;
    data['createddate'] = this.createddate;

    return data;
  }
}