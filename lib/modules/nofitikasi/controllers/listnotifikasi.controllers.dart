
import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:notifwp/config/api.config.dart';
import 'package:notifwp/config/modules.config.dart';
import 'package:notifwp/config/routes.config.dart';
import 'package:notifwp/modules/nofitikasi/models/notif.models.dart';

class ListNotifikasiControllers extends GetxController{
  List<NotifModel> data = [];
  bool loadingProses = false;

  void getListNotifikasi(String user_id) async{
    this.loadingProses = true;
    update();

    Map<String, dynamic> params = {};
    params["user_id"] = user_id;

    var dio = new Dio();
    var request = await dio.post(Api.route[ModulesConfig.MODULE_NOTIF][Routes.GETDATA], queryParameters: params);


    // print("DATA WP LIST ${request.body}");
    if(request.statusCode == 200){
      this.loadingProses = false;
      update();
      // print("REQUES DATA ${request.data['data'][0]}");
      var data = request.data;

      for(var val in data["data"]){
        // val['data'] = val['data'].toString();
        this.data.add(NotifModel.fromJson(val));
        // print("VALUE DATA ${val}");
        // NotifModel mdb = new NotifModel();
        // mdb.status = val["status"];
        // mdb.content = val["content"];
        // this.data.add(mdb);
      }

    }else{
      this.loadingProses = false;
      update();

      Get.snackbar("Informasi", "Gagal Memuat Data", backgroundColor: Colors.redAccent, colorText: Colors.white);
    }


    this.update();
  }

}