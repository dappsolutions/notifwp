

import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:notifwp/config/api.config.dart';
import 'package:notifwp/config/modules.config.dart';
import 'package:notifwp/config/routes.config.dart';
import 'package:notifwp/modules/response/models/message.models.dart';
import 'package:onesignal_flutter/onesignal_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';

class NotifikasiControllers extends GetxController{
  String username = "";
  String user_id = "";
  String OneSignalIdPlayer = "";

  void getDataSession() async{
    SharedPreferences pref = await SharedPreferences.getInstance();
    this.username = pref.getString("username");
    this.user_id = pref.getString("user_id");
    this.update();
  }

  void simpanNotif(String content, Map<String, dynamic> data) async{

    Map<String, dynamic> params = {};
    params["content"] = content;
    params["data"] = data.toString();
    params['user_id'] = this.user_id;

    var dio = new Dio();
    var request = await dio.post(Api.route[ModulesConfig.MODULE_NOTIF][Routes.SIMPAN], queryParameters: params);

    if(request.statusCode == 200){
      var data = json.decode(request.data);
      // print("DATA ${data}");
      MessageModel respMessage = MessageModel.fromJson(data);
      if(respMessage.is_valid == "1"){
        Get.snackbar("Informasi", respMessage.message, backgroundColor: Colors.green, colorText: Colors.white);
      }else{
        Get.snackbar("Informasi", respMessage.message, backgroundColor: Colors.redAccent, colorText: Colors.white);
      }
    }else{
      Get.snackbar("Informasi", "Gagal Menyinkronkan Data", backgroundColor: Colors.redAccent, colorText: Colors.white);
    }
  }

  void getSubscriptionsId() async{
    var status = await OneSignal.shared.getPermissionSubscriptionState();
    var playerId = "";
    if(status.subscriptionStatus.subscribed){
      playerId = status.subscriptionStatus.userId;
    }

    this.OneSignalIdPlayer = playerId;
    this.update();
  }

  void signOut() async{
    SharedPreferences pref = await SharedPreferences.getInstance();
    pref.clear();
  }
}