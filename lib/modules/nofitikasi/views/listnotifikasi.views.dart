import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:notifwp/modules/nofitikasi/controllers/listnotifikasi.controllers.dart';
import 'package:notifwp/ui/Uicolor.dart';

class ListNotifikasiViews extends StatefulWidget {
  String user_id;
  ListNotifikasiViews({this.user_id});

  // const ListNotifikasiViews({Key key}) : super(key: key);

  @override
  _ListNotifikasiViewsState createState() => _ListNotifikasiViewsState();
}

class _ListNotifikasiViewsState extends State<ListNotifikasiViews> {
  ListNotifikasiControllers controller;

  @override
  void initState() {
    controller = new ListNotifikasiControllers();
    controller.getListNotifikasi(widget.user_id);

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: Text("DAFTAR NOTIFIKASI MASUK", style: TextStyle(color: Uicolor.hexToColor(Uicolor.white)),),
          backgroundColor: Uicolor.hexToColor(Uicolor.green),
          elevation: 0,
          centerTitle: true,
        ),
        body: SingleChildScrollView(
            child: Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Container(
                      child: GetBuilder<ListNotifikasiControllers>(
                        init: controller,
                        builder: (params){
                          if(!params.loadingProses){
                            if(params.data.length > 0){
                              return ListView.builder(
                                itemCount: params.data.length,
                                shrinkWrap: true,
                                physics: ClampingScrollPhysics(),
                                itemBuilder: (context, index){
                                  var data = params.data[index];
                                  return Container(
                                      height: 300,
                                      margin: EdgeInsets.only(left: 16, right: 18, top: 8),
                                      child: GestureDetector(
                                        child: Card(
                                          elevation: 0,
                                          color: Colors.amberAccent,
                                          child: Container(
                                            padding: EdgeInsets.all(16),
                                            child: Column(
                                              mainAxisAlignment: MainAxisAlignment.start,
                                              children: <Widget>[
                                                Container(
                                                    child: Icon(Icons.notifications, color: Colors.black,),
                                                  alignment: Alignment.topLeft,
                                                ),
                                                Container(
                                                  child: Text(data.content),
                                                  alignment: Alignment.topLeft,
                                                  padding: EdgeInsets.all(8),
                                                ),
                                                Container(
                                                  child: Text(data.createddate),
                                                  alignment: Alignment.topLeft,
                                                  padding: EdgeInsets.all(8),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                        onTap: ()=>{

                                        },
                                      )
                                  );
                                },
                              );
                            }

                            if(params.data.length == 0){
                              return Center(
                                child: Text("Tidak ada data ditemukan"),
                              );
                            }
                          }


                          return Center(
                            child: CircularProgressIndicator(),
                          );
                        },
                      )
                  ),
                ],
              ),
            )
        ),
    );
  }
}
