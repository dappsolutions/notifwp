import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:notifwp/modules/main/views/main.views.dart';
import 'package:notifwp/modules/nofitikasi/controllers/notifikasi.controllers.dart';
import 'package:notifwp/modules/nofitikasi/views/listnotifikasi.views.dart';
import 'package:notifwp/ui/Uicolor.dart';
import 'package:onesignal_flutter/onesignal_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';

class NotifikasiViews extends StatefulWidget {
  @override
  _NotifikasiViewsState createState() => _NotifikasiViewsState();
}

class _NotifikasiViewsState extends State<NotifikasiViews> {
  String title = "";
  String content = "";
  Map<String, dynamic> data = {};

  NotifikasiControllers controllers;

  @override
  void initState() {
    super.initState();
    controllers = new NotifikasiControllers();
    controllers.getDataSession();
    controllers.getSubscriptionsId();

    OneSignal.shared.setNotificationReceivedHandler((OSNotification notification) {
      setState(() {
        this.title = notification.payload.title;
        this.content = notification.payload.body;
        this.data = notification.payload.additionalData;
        controllers.simpanNotif(this.content, this.data);
        // print("notification ${this.data['nowp']}");
      });
    });

    OneSignal.shared.setNotificationOpenedHandler((OSNotificationOpenedResult result) {
      setState(() {
        this.title = result.notification.payload.title;
        this.content = result.notification.payload.body;
        this.data = result.notification.payload.additionalData;

        controllers.simpanNotif(this.content, this.data);
      });
      // print("ini ditap ${this.data}");
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Notifikasi Terbaru", style: TextStyle(color: Uicolor.hexToColor(Uicolor.white)),),
        backgroundColor: Uicolor.hexToColor(Uicolor.green),
        elevation: 0,
        centerTitle: true,
        automaticallyImplyLeading: false,
        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.sort, color: Uicolor.hexToColor(Uicolor.white),),
              onPressed: (){
                Get.to(ListNotifikasiViews(
                  user_id: controllers.user_id,
                ));
              }
          )
        ],
      ),
      body: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
          Container(
              height: 350,
                margin: EdgeInsets.only(left: 16, right: 18, top: 16),
                child: GestureDetector(
                  child: Card(
                    elevation: 0,
                    color: Uicolor.hexToColor(Uicolor.green_smooth),
                    child: Container(
                      padding: EdgeInsets.all(16),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            alignment: Alignment.topLeft,
                              child: Icon(Icons.notifications_active, color: Uicolor.hexToColor(Uicolor.green),)
                          ),
                          // Container(
                          //   padding: EdgeInsets.all(8),
                          //   alignment: Alignment.topLeft,
                          //   child: GetBuilder<NotifikasiControllers>(
                          //     init: controllers,
                          //     builder: (params){
                          //       return Text("Username : ${params.username}", style: TextStyle(fontSize: 18),);
                          //     },
                          //   ),
                          // ),
                          Container(
                            padding: EdgeInsets.all(8),
                            alignment: Alignment.topLeft,
                            child: Text(this.title, style: TextStyle(fontSize: 20),),
                          ),
                          Container(
                            padding: EdgeInsets.all(8),
                            child: this.data.isNotEmpty ? Text(this.content+" - "+this.data['nowp'], style: TextStyle(fontSize: 16)) : Text("Belum ada notifikasi masuk", style: TextStyle(fontSize: 14),),
                          ),
                          Container(
                            padding: EdgeInsets.all(8),
                            alignment: Alignment.topLeft,
                            child: this.data.isNotEmpty ? Text(DateTime.now().toString(), style: TextStyle(fontSize: 16)) : Text("", style: TextStyle(fontSize: 14),),
                          ),
                        ],
                      ),
                    ),
                  ),
                  onTap: ()=>{

                  },
                )
            ),
          ],
        ),
        // child: StreamBuilder(
        //   stream: socketStream.getResponse,
        //   builder: (context, snapshot){
        //       if(snapshot.data != null){
        //         return Text(snapshot.data);
        //       }
        //
        //       return Text("-");
        //   },
        // )
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.subdirectory_arrow_left),
        backgroundColor: Colors.amberAccent,
        onPressed: () async{
          controllers.signOut();
          Future.delayed(Duration(seconds: 2));
          Navigator.of(context).pop();
          Get.to(MainViews());
        },
      ),
    );
  }
}
