

class MessageModel{
  String is_valid;
  String message;
  MessageModel({this.is_valid, this.message});

  MessageModel.fromJson(Map<String, dynamic> json){
    this.is_valid = json['is_valid'];
    this.message = json['message'];
  }
}