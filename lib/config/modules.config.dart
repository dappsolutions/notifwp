
class ModulesConfig {
  static String MODULE_LOGIN = "login";
  static String MODULE_NOTIF = "notif";
  static String MODULE_PERMIT = "permit";
  static String MODULE_GRAFIK = "grafik";
  static String MODULE_SIDAK = "sidak";
  static String MODULE_BERKAS = "berkas";
}