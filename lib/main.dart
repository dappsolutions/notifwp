import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:notifwp/modules/main/views/main.views.dart';
import 'package:notifwp/streams/socket.stream.dart';
import 'package:provider/provider.dart';
import 'package:socket_io_client/socket_io_client.dart' as IO;
import 'package:socket_io_client/socket_io_client.dart';
import 'package:onesignal_flutter/onesignal_flutter.dart';

import 'config/connectivity.dart';

void main() {
  // connectAndListen();
  WidgetsFlutterBinding.ensureInitialized();
  OneSignal.shared.init(
      "ccd27271-600a-451b-9862-2a5d3b732d05",
      iOSSettings: null
  );
  OneSignal.shared.setInFocusDisplayType(OSNotificationDisplayType.notification);
  runApp(MyApp());
}

class MyApp extends StatelessWidget {


  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
        create: (context) {
          ConnectivityChangeNotifier changeNotifier =
          ConnectivityChangeNotifier();
          //Inital load is an async function, can use FutureBuilder to show loading
          //screen while this function running. This is not covered in this tutorial
          changeNotifier.initialLoad();
          return changeNotifier;
        },
        child: GetMaterialApp(
          title: 'Notifkasi Working Permit',
          debugShowCheckedModeBanner: false,
          theme: ThemeData(
            primarySwatch: Colors.blue,
            visualDensity: VisualDensity.adaptivePlatformDensity,
          ),
          // home: MyHomePage(title: 'Notifkasi Working Permit'),
          home: MainViews(),
        )
    );
  }
}

SocketStream socketStream = SocketStream();

//STEP2: Add this function in main function in main.dart file and add incoming data to the stream
void connectAndListen(){
  IO.Socket socket = IO.io('http://121.100.16.218:3000',
      OptionBuilder()
          .setTransports(['websocket']).build());
  // print("DATA SOCKET ${socket}");

  socket.onConnect((_) {
    print('connect');
    socket.emit('msg', 'test');
  });

  //When an event recieved from server, data is added to the stream
  socket.on('check_rfid', (data) => socketStream.addResponse);
  socket.onDisconnect((_) => print('disconnect'));

}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  String title = "";
  String content = "";
  Map<String, dynamic> data = {};

  @override
  void initState() {
    super.initState();

    OneSignal.shared.setNotificationReceivedHandler((OSNotification notification) {
      setState(() {
        this.title = notification.payload.title;
        this.content = notification.payload.body;
        this.data = notification.payload.additionalData;

        print("notification ${this.data['nowp']}");
      });
    });

    OneSignal.shared.setNotificationOpenedHandler((OSNotificationOpenedResult result) {
      setState(() {
        this.title = result.notification.payload.title;
        this.content = result.notification.payload.body;
        this.data = result.notification.payload.additionalData;
      });
      // print("ini ditap ${this.data}");
    });
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(this.title),
            SizedBox(
              height: 20,
            ),
            this.data.isNotEmpty ? Text(this.content+" - "+this.data['nowp']) : Text(this.content),
          ],
        ),
        // child: StreamBuilder(
        //   stream: socketStream.getResponse,
        //   builder: (context, snapshot){
        //       if(snapshot.data != null){
        //         return Text(snapshot.data);
        //       }
        //
        //       return Text("-");
        //   },
        // )
      ),
    );
  }
}
